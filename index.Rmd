---
title: "ANR SpatialTreeP"

output: 
  flexdashboard::flex_dashboard:
    orientation: columns
    theme: 
         version: 4
         bootswatch: minty
         navbar-bg: "#00b700ff"
    social: menu
    source_code: embed

---

```{r setup, include=FALSE}
library(flexdashboard)
library(mapview)
library(leaflet)
library(sf)
library(plotly)
library(tmap)
library(ggplot2)
library(terra)
library(tmaptools)

```

# **Sujet de l'étude** {.sidebar}

Mobilité de la limite supérieure de la forêt subalpine dans les Pyrénées : pour une meilleure compréhension des interactions entre changement climatique, mutations anthropiques et effets de site

<center>

![](data/images/logo_st_fin.png){width=80%}
</center>

# Le projet {data-orientation=rows, data-icon="fa-tree"}

##  {.tabset} {data-width=60, .tabset}

### **Résumé du projet**

La limite supérieure de la forêt subalpine représente une discontinuité paysagère majeure, dont la dynamique spatiale est associée à de multiples enjeux scientifiques. A la fois marqueur de changements environnementaux, hotspot de biodiversité et facteur d’atténuation des aléas gravitaires et de l’érosion des sols, cet écotone constitue un objet d’étude particulièrement digne d’intérêt.

Néanmoins, la richesse de cet objet se traduit par une certaine complexité que l’état actuel des connaissances ne permet pas encore d’appréhender parfaitement. La dynamique de l’écotone est en effet caractérisée par une forte hétérogénéité spatiale et une diversité de processus écologiques (régression, progression, densification sans progression, changements de forme, etc.). Cette hétérogénéité reflète le caractère multidimensionnel, complexe (intégrant des interactions) et multiscalaire des facteurs à l’œuvre.

En se concentrant sur les Pyrénées, une chaîne de montagne géographiquement hétérogène et très anthropisée, ce projet vise à démêler ces structures complexes en privilégiant un cadre intégré, interdisciplinaire et spatialement explicite. La principale hypothèse de recherche consiste à supposer que les effets de site jouent un rôle modérateur primordial dans la relation entre le changement climatique et le changement de l’écotone, et sont à l’origine d’une hiérarchisation spatialement différenciée des déterminants de la dynamique écotonale, au-delà de la dichotomie classique confrontant facteurs anthropiques et climatiques à l’échelle régionale.

L’originalité du projet tient d’une part de sa structure basée sur l’articulation des échelles géographiques. Le WP1 consiste en la constitution d’une base de données à l’échelle du massif sur l’évolution de la treeline et ses facteurs, largement basées sur la géomatique, à partir de fenêtres d’observations réparties tout le long de la treeline préalablement digitalisée par le biais de classifications automatiques d’images. Le WP2, articulant le régional et le local, procédera à l’analyse spatiale quantitative de cette base de données et permettra d’identifier 20 sites à fort intérêt scientifique. Ces 20 sites feront ensuite l’objet d’une analyse approfondie dans le WP3, permettant de collecter des données locales non discernables ou quantifiables à l’échelle régionale.

Ces analyses locales viseront notamment à proposer une reconstruction des trajectoires anthropiques susceptibles d’avoir interféré avec la dynamique écotonale sur ces sites, ainsi qu’une série de mesures écologiques pour caractériser les habitats et leur rôle fonctionnel dans l’évolution de la treeline. L’autre point fort du projet relève du caractère multidisciplinaire du consortium, monté pour répondre précisément à ces objectifs. L’équipe comprend des jeunes chercheurs et d’autres plus confirmés, principalement en géographie humaine et physique, en histoire environnementale et en écologie.

L’ensemble des résultats de ce projet (évolution de la treeline pyrénéenne et identification de ses facteurs à différentes échelles) sera disséminé en dehors du monde académique, notamment à travers une cartographie interactive en ligne. Cette dissémination et l’identification de sites à fort intérêt scientifique permettront de proposer des recommandations localement ciblées aux décideurs publics (parc nationaux et régionaux, ONF, collectivités territoriales) pour mieux gérer ces espaces de haute montagne à fort enjeu scientifique.

### **Project's abstract**

The upper limit of the subalpine forest represents a major landscape discontinuity, whose the spatial dynamics is associated with multiple scientific issues. As a marker of environmental change, a biodiversity hotspot and a key factor in the mitigation of gravity hazards and soil erosion, this ecotone is a particularly interesting scientific object.

Nevertheless, the richness of this object is reflected in a certain complexity that the current state of knowledge does not yet allow for a perfect understanding.
The dynamics of the ecotone is characterised by strong spatial heterogeneity and a diversity of ecological processes (regression, progression, densification without progression, changes in form, etc.). This heterogeneity reflects the multidimensional, complex (integrating interactions) and multiscale nature of the factors at work. By focusing on the Pyrenees, a geographically heterogeneous and highly anthropised mountain range, this project aims to unravel these complex structures by favouring an integrated, interdisciplinary and spatially explicit framework.

The main research hypothesis is to assume that site effects play a primary moderating role in the relationship between climate change and ecotone change, and are at the origin of a spatially differentiated hierarchy of the determinants of ecotonal dynamics, beyond the classic dichotomy confronting anthropic and climatic factors at the regional scale only. The originality of the project lies on the one hand in its structure based on the articulation of geographical scales. WP1 consists in the constitution of a regional scale database quantifying the evolution of the treeline (since the 1950s) and its factors, largely based on geomatics, from observation plots spread all along the treeline (previously digitised through automatic image classifications). The WP2, articulating regional and local scales, will consist in computing the quantitative spatial analysis of this database and will allow the identification of 20 sites of high scientific interest. These 20 sites will then be studied through in-depth analysis in WP3, allowing the collection of local data that cannot be discerned or quantified at a regional scale.

These local analyses will aim to provide a reconstruction of the anthropogenic trajectories likely to have interfered with the ecotonal dynamics on these sites, as well as a series of ecological measures to characterise the habitats and their functional role in the evolution of the treeline. The other strength of the project is the multidisciplinary nature of the consortium, which was set up to meet these objectives.

The team includes young and more experienced researchers, mainly in human and physical geography, environmental history and ecology. All the results of this project (evolution of the Pyrenean treeline and identification of its factors at different scales) will be disseminated outside the academic world, in particular through an online interactive cartography. This dissemination and the identification of sites of high scientific interest will make it possible to propose locally targeted recommendations to public decision-makers (national and regional parks, ONF, local authorities) for better management of these high mountain areas with strong scientific stakes.


<!-- ### {data-width=150} -->
<!-- ![](data/images/photo2.png) -->

## {data-width=40}

### **Informations générales** {data-height=25}

- Financé par l'ANR [https://anr.fr/Projet-ANR-21-CE03-0002](https://anr.fr/Projet-ANR-21-CE03-0002)
- Date de début du projet : Janvier 2022
- Durée : 36 mois
- Budget : 268 k€

<center>
![Agence nationale de la recherche](data/images/logo_anr_small.png){width=45%}

</center>

### {data-height=75}



<center>

![Pin à crochets (*Pinus uncinata* Ramond ex. DC), massif du Puigmal, 2150 m](data/images/photo1.png){width=85%}

</center>


# Contexte géographique {data-icon="fa-map"}

## {.tabset}

### Altitude > 1500m

```{r, eval = TRUE, echo = FALSE, message=FALSE, warning=FALSE}

pyrenees <- st_read("data/vecteur/pyrenees_1500m.shp", quiet = T)
sommets =  st_read("data/vecteur/sommets.shp", quiet = T)
MNT <- rast("data/raster/MNT.tif")

tmap_mode("view")

tm_basemap(leaflet::providers$OpenStreetMap) + 
  tm_shape(pyrenees) + 
  tm_borders(col = "red", lwd = 3) + 
  tm_view(set.view = c(0.94, 42.62, 8.45)) + 
  tm_shape(sommets) + 
  tm_bubbles(col = "black", 
             size = 1, 
             shape = 3, 
             id = "nom") +
  tm_scale_bar() +
  tm_compass() +
  tm_shape(MNT) +
  tm_raster(style = "cont", 
            title = "Altitude (m)",
            palette = "-Spectral")

```

### Pente

```{r eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}

Pente <- rast("data/raster/Pente.tif")

tmap_mode("view")

tm_shape(Pente) + 
  tm_raster(title = "Pente",) +
  tm_view(set.view = c(0.94, 42.62, 8.45)) +
  tm_shape(sommets) + 
  tm_bubbles(col = "black", 
             size = 1, 
             shape = 3, 
             id = "nom") +
  tm_scale_bar() +
  tm_compass()

```

### Orientation

```{r eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}

Exposition <- rast("data/raster/Exposition.tif")

tmap_mode("view")

tm_shape(Exposition) + 
  tm_raster(title = "Exposition",) +
  tm_view(set.view = c(0.94, 42.62, 8.45)) +
  tm_shape(sommets) + 
  tm_bubbles(col = "black", 
             size = 1, 
             shape = 3, 
             id = "nom") +
  tm_scale_bar() +
  tm_compass()

```

### TWI

```{r eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}

TWI <- rast("data/raster/TWI.tif")

tmap_mode("view")

tm_shape(TWI) + 
  tm_raster(palette = get_brewer_pal("Blues", n = 7, plot=FALSE),
            title = "TWI") +
  tm_view(set.view = c(0.94, 42.62, 8.45)) +
  tm_shape(sommets) + 
  tm_bubbles(col = "black", 
             size = 1, 
             shape = 3, 
             id = "nom") +
  tm_scale_bar() +
  tm_compass()

```

### Geomorphons

```{r eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}

Geomorphons <- rast("data/raster/Geomorphons.tif")

tmap_mode("view")

tm_shape(Geomorphons) + 
  tm_raster(style = "cat",
            title = "Geomorphons",
            labels = c("Peak", "Ridge", "Shoulder", "Spur", "Slope", "Footslope", "Flat", "Hollow", "Valley", "Pit"),
            palette = c("magenta", "red", "orange", "yellow", "grey40",  "grey70", "grey90", "skyblue1", "dodgerblue", "royalblue3")) +
  tm_view(set.view = c(0.94, 42.62, 8.45)) +
  tm_layout(legend.outside = TRUE) +
  tm_shape(sommets) + 
  tm_bubbles(col = "black", 
             size = 1, 
             shape = 3, 
             id = "nom") +
  tm_scale_bar() +
  tm_compass()

```

# Premiers résultats {data-icon="fa-chart-bar"}

## Row {data-width=500}
-----------------------------------------------------------------------

### **Cartographie des parcelles**

```{r, eval = TRUE, echo = FALSE, message=FALSE, warning=FALSE}
parcelles <- st_read("data/vecteur/parcelles.shp", quiet = T)
vues <- parcelles %>% filter(Terrain == "VU") %>% st_centroid()
parcelles$shift2 <- cut(parcelles$shift,
  include.lowest = TRUE,
  right = FALSE,
  dig.lab = 4,
  breaks = c(-212, -30, 40, 80, 308)
)

# map <- mapview(parcelles$shftQrt, legend=F, col.regions="red", 
#         map.types = c("OpenTopoMap"))
# setView(map@map, lng = 1.84, lat = 42.6, zoom=12)
tmap_mode("view")

vues <- parcelles %>% filter(Terrain == "VU") %>% st_centroid()
clustExt <- parcelles %>% filter(change %in% c("1_3","3_1")) %>% st_centroid()

tm_basemap(leaflet::providers$OpenTopoMap) + 
  tm_shape(parcelles) + 
  tm_polygons(col = "shift2", palette = "-RdBu", alpha = 0.8) + 
  tm_view(set.view = c(1.84, 42.62, 12)) + 
  tm_shape(clustExt) + 
  tm_dots(col = "change", palette = "Set1", size = .08) +
  tm_shape(vues) + 
  tm_dots() +
  tm_scale_bar()

```

{data-width=400, .tabset}
-----------------------------------------------------------------------

### **Evolution des altitudes entre 1956 et 2015**

```{r}
plot_ly(parcelles, x = ~shift) %>% add_histogram(marker = list(line = list(color = "grey80",width = .5))) %>% layout(xaxis = list(title = "Changement d'altitude en m"))
```

### **Distribution des quartiles**

```{r}
g <- ggplot(data = parcelles, aes(y=shift, x=shift2)) + 
  geom_violin(fill = "grey40", colour=NA) + 
  labs(y = "Variation altitudinale", x="Quartiles")
ggplotly(g)
```


-----------------------------------------------------------------------

### **Evolution des densités entre 1956 et 2015**

```{r}
plot_ly(parcelles, x = ~DENS) %>% add_histogram(marker = list(line = list(color = "grey80",width = .5))) %>% layout(xaxis = list(title = "Changement de densité en %"))
```

### **Relation variation altitude vs densité**

```{r}
ggplot(data = parcelles, aes(y=shift, x=DENS)) + geom_point(col="grey60", alpha = .9) +
  geom_smooth(method = "loess", color = "red", se = TRUE) + 
  labs(y = "Variation d'altitudes", x="Variation de densité") + 
  theme_grey()
```

# L'équipe {data-icon="fa-users"}

## Rows {data-width=500}

###

*Le consortium est composé de biogéographes, de géographes ruralistes, de géomorphologues, d'écologues, de climatologues et d'historiens de l'environnement.*

```{r, eval = TRUE, echo = FALSE, message=FALSE, warning=FALSE}
labos <- read.csv("data/labos.csv")
labos_sf <- labos %>% st_as_sf(coords = c("X","Y"), crs = 4326)  
tm_shape(labos_sf) +
  tm_dots(col = "red") +
  tm_basemap(leaflet::providers$OpenStreetMap)

```

## Rows {data-width=250}

### [**UMR LADYSS (CNRS)**](https://www.ladyss.com/) {data-height=60}

<!-- # ```{r picture, echo = F, out.width = '.5%'} -->
<!-- # knitr::include_graphics("logo_ladyss.png") -->
<!-- # ``` -->

- [Céline Clauzel](https://www.ladyss.com/clauzel-celine), MCF-HDR (Université de Paris)
- [Thierry Feuillet](https://geographie.univ-paris8.fr/?thierry-feuillet), MCF-HDR (Université Paris 8)
- [Vincent Godard](https://www.ladyss.com/godard-vincent), PR (Université Paris 8)
- [Bénédicte McGregor](https://www.ladyss.com/macgregor-benedicte), AI CNRS
- [Johan Milian](https://www.ladyss.com/milian-johan), MCF (Université Paris 8)
- [Olivier Theureaux](https://www.researchgate.net/profile/Olivier-Theureaux), Stagiaire

### [**UMR DYNAFOR (INRAE)**](https://www.dynafor.fr/) {data-height=35}


- [Luc Barbaro](https://www.dynafor.fr/barbaro-luc), CR-HDR
- [François Calatayud](https://www.dynafor.fr/calatayud-francois), IR
- [Laurent Larrieu](https://www.researchgate.net/profile/Laurent-Larrieu-2), CR
- [David Sheeren](https://dsheeren.github.io/#contact), MCF (ENSAT)

### [**UMR CEFE (CNRS/IRD/INRAE)**](https://www.cefe.cnrs.fr/fr/) {data-height=5}

- [Xavier Morin](https://www.cefe.cnrs.fr/fr/recherche/ef/forecast/829-c/288-xavier-morin), CR-HDR

## Row {data-width=250}

### [**EA CHCSC**](https://www.chcsc.uvsq.fr/) 

- [Steve Hagimont](https://www.chcsc.uvsq.fr/m-steve-hagimont), MCF (UVSQ)

### [**UMR HNHP (CNRS/MNHN)**](https://hnhp.mnhn.fr/fr)

- [Magali Delmas](https://hnhp.mnhn.fr/fr/annuaire/magali-delmas-7027), MCF-HDR (Université de Perpignan)

### [**EA Pléiade**](https://pleiade.univ-paris13.fr/)

- [Frédéric Alexandre](https://pleiade.univ-paris13.fr/profil/frederic.alexandre/), PR (Université Paris 13)
- [Déborah Birre](https://www.researchgate.net/profile/Deborah-Birre), doctorante (Université Paris 13)

### [**Universidad Autónoma de Madrid**](https://www.uam.es/uam/inicio)

- [Roberto Serrano](https://www.researchgate.net/profile/Roberto-Serrano-Notivoli), Ass. Prof. (U. Madrid)


# Publications {data-icon="fa-book-open"}

### **Articles scientifiques**
- Feuillet T., Birre D., Milian J., Godard V., Clauzel C., Serrano-Notivoli R. 2020. [Spatial dynamics of alpine treelines under global warming : what explains the mismatch between tree densification and altitudinal upward shifts at the treeline ecotone?](https://onlinelibrary.wiley.com/doi/abs/10.1111/jbi.13779?af=R) *Journal of biogeography*, 47(5) : 1056-1068.

### **Conférences**


